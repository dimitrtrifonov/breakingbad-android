# BreakingBad Android Application

Edit 6/2023 - BreakingBad API does not work anymore, so the application fetches data from a JSON file stored locally.
BreakingBad-Android is an Android Application that fetches API endpoint https://breakingbadapi.com/api/characters
and displays data in a list. The data can be filtered by characters' name and season appearance. 
Character details are displayed on the second screen.

## Architecture
The application is created using MVVM pattern, Dagger, Retrofit, LiveData.