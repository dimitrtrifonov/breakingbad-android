package com.example.breakingbad.di

import com.example.breakingbad.view.viewmodel.CharactersListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface NetworkComponent {
    fun inject(charactersListViewModel: CharactersListViewModel)
}
