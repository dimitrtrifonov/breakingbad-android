package com.example.breakingbad.view.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.breakingbad.R
import com.example.breakingbad.model.Character
import com.google.android.material.textview.MaterialTextView
import java.util.*
import kotlin.collections.ArrayList

class CharactersListRecyclerViewAdapter(
    private val onItemClickListener: ((Character?) -> Unit)
) : RecyclerView.Adapter<CharactersListRecyclerViewAdapter.ViewHolder>(), Filterable {

    private var initList = ArrayList<Character>()
    private var filteredList = ArrayList<Character>()
    private var isFilterName: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)

        return ViewHolder(view)
    }

    fun filterByName(_isFilterName: Boolean) {
        isFilterName = _isFilterName
    }

    fun initList(_initList: List<Character>) {
        initList = _initList as ArrayList<Character>
        filteredList = initList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.itemView.context).load(filteredList[position].img)
            .diskCacheStrategy(DiskCacheStrategy.ALL).listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBarLoading.visibility = View.GONE
                    return false
                }
            }).into(holder.imageView)

        holder.textView.text = filteredList[position].name
        holder.itemView.setOnClickListener { onItemClickListener.invoke(filteredList[position]) }
    }

    inner class ViewHolder(itemsView: View) : RecyclerView.ViewHolder(itemsView) {
        val imageView: ImageView = itemsView.findViewById(R.id.image)
        val textView: MaterialTextView = itemsView.findViewById(R.id.name)
        val progressBarLoading: ProgressBar = itemsView.findViewById(R.id.progress_bar_loading)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {

                val filterResults = FilterResults()
                filterResults.values =
                    if (isFilterName) getFilteredByNameList(constraint) else getFilteredBySeasonList(
                        constraint
                    )
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = (results?.values as? ArrayList<Character>) ?: ArrayList()
                notifyDataSetChanged()
            }
        }
    }

    fun getFilteredByNameList(constraint: CharSequence?): ArrayList<Character> {
        val resultList = ArrayList<Character>()
        for (character in initList) {
            if (isCharSequenceInCharacterName(character, constraint)) {
                resultList.add(character)
            }
        }
        return resultList
    }

    private fun isCharSequenceInCharacterName(character: Character, constraint: CharSequence?): Boolean {
        return character.name?.toLowerCase(Locale.ROOT)?.contains(
            constraint.toString().toLowerCase(
                Locale.ROOT
            )
        ) == true
    }

    fun getFilteredBySeasonList(constraint: CharSequence?): ArrayList<Character> {
        val resultList = ArrayList<Character>()
        for (character in initList) {
            if (isCharacterInSeason(constraint.toString().toInt(),character)
            ) {
                resultList.add(character)
            }
        }
        return resultList
    }

    private fun isCharacterInSeason(seasonNumber: Int,character: Character): Boolean {

        character.appearance?.forEach{ value ->
            if(value==seasonNumber)return true
        }
        return false
    }
}