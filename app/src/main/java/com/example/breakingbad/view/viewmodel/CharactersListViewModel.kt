package com.example.breakingbad.view.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.breakingbad.di.DaggerNetworkComponent
import com.example.breakingbad.network.ApiService
import com.example.breakingbad.network.ApiServiceMock
import com.example.breakingbad.view.utils.CharactersListResult
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.InputStream
import javax.inject.Inject

class CharactersListViewModel() : ViewModel() {

    constructor(_apiService: ApiService) : this() {
        apiService = _apiService
    }

    @Inject
    lateinit var apiService: ApiService

    private val _charactersListResult = MutableLiveData<CharactersListResult>()
    val charactersListResult: LiveData<CharactersListResult> = _charactersListResult

    init {
        val networkComponent = DaggerNetworkComponent.builder().build()
        networkComponent.inject(this)
    }

    @SuppressLint("CheckResult")
    fun charactersRequest(inputStream: InputStream? = null) {
        _charactersListResult.value =
            CharactersListResult(isLoading = true)
        try {
            apiService.characters().subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
            ).subscribe({
                _charactersListResult.value =
                    CharactersListResult(
                        charactersResponse = it
                    )
            }, {
                getMockedData(inputStream)
            })
        } catch (e: Exception) {
            e.toString()
        }
    }

    private fun getMockedData(inputStream: InputStream?) {
        ApiServiceMock.characters(inputStream).subscribe(object :
            SingleObserver<List<com.example.breakingbad.model.Character>> {
            override fun onSubscribe(d: Disposable) {
            }

            override fun onError(e: Throwable) {
            }

            override fun onSuccess(charactersList: List<com.example.breakingbad.model.Character>) {
                _charactersListResult.value =
                    CharactersListResult(
                        charactersResponse = charactersList
                    )
            }
        });
    }
}