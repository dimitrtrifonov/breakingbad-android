package com.example.breakingbad.view.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.breakingbad.R
import com.example.breakingbad.model.Character
import com.google.android.material.textview.MaterialTextView

class CharacterDetailsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_character_details, container, false)

        val character: Character? = this.arguments?.getParcelable(getString(R.string.character))

        setImage(character, root)

        val characterName = "Name: " + character?.name.toString()
        root.findViewById<MaterialTextView>(R.id.name).text = characterName

        val occupationCommaSeparatedString =
            character?.occupation?.joinToString(separator = ", ") { it }
        val characterOccupation = "Occupation: $occupationCommaSeparatedString"
        root.findViewById<MaterialTextView>(R.id.occupation).text = characterOccupation

        val characterStatus = "Status: " + character?.status.toString()
        root.findViewById<MaterialTextView>(R.id.status).text = characterStatus

        val characterNickname = "Nickname: " + character?.nickname.toString()
        root.findViewById<MaterialTextView>(R.id.nickname).text = characterNickname

        val commaSeparatedString =
            character?.appearance?.joinToString(separator = ", ") { it.toString() }
        val characterAppearance = "Season Appearance: $commaSeparatedString"
        root.findViewById<MaterialTextView>(R.id.appearance).text = characterAppearance

        return root
    }

    private fun setImage(character: Character?, root: View) {

        Glide.with(requireActivity()).load(character?.img).diskCacheStrategy(DiskCacheStrategy.ALL)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    root.findViewById<ProgressBar>(R.id.progress_bar_loading).visibility = View.GONE
                    return false
                }
            }).into(root.findViewById(R.id.image))
    }
}

