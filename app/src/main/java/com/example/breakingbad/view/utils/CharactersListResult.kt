package com.example.breakingbad.view.utils

import com.example.breakingbad.model.Character

data class CharactersListResult(
    val charactersResponse: List<Character>? = null,
    val charactersError: Throwable? = null,
    val isLoading: Boolean? = null
)