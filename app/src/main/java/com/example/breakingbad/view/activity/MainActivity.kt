package com.example.breakingbad.view.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import com.example.breakingbad.R
import com.example.breakingbad.view.fragment.CharactersListFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menu: Menu
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById<MaterialToolbar>(R.id.toolbar))

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(navController.graph)
        navController.addOnDestinationChangedListener(destinationChangedListener)

        setupActionBarWithNavController(this, navController)
    }

    private var destinationChangedListener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            setMenuIconVisibility(destination.id != R.id.navigation_character_details_fragment)
        }

    private fun setMenuIconVisibility(isVisible: Boolean){
        if (this::menu.isInitialized) {
            menu.findItem(R.id.more).isVisible = isVisible
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_app_bar, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> navController.popBackStack()
            R.id.more -> getAlertDialog().show()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getAlertDialog(
    ): AlertDialog {

         val view: View = layoutInflater.inflate(R.layout.layout_alert_dailog, null)

        return MaterialAlertDialogBuilder(
            this
        ).setTitle(getString(R.string.please_enter_season_number))
            .setView(view)
            .setPositiveButton(
                getString(R.string.ok)
            ) { _, _ ->
                run {
                    val inputString = view.findViewById<TextInputEditText>(R.id.edit_text).text.toString()
                    if(inputString.isNotBlank()){
                        updateItemsRecyclerView(inputString.toInt())
                    }
                }
            }.create()
    }

    private fun updateItemsRecyclerView(seasonNumber: Int) {
        if (navController.currentBackStackEntry?.destination?.id == R.id.navigation_characters_list_fragment) {
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
            navHostFragment.childFragmentManager.primaryNavigationFragment?.let {
                (it as CharactersListFragment).filterBySeason(seasonNumber)
            }
        }
    }
}