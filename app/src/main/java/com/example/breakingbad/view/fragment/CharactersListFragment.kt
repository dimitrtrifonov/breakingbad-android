package com.example.breakingbad.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.breakingbad.R
import com.example.breakingbad.model.Character
import com.example.breakingbad.view.adapter.CharactersListRecyclerViewAdapter
import com.example.breakingbad.view.utils.PreCachingLayoutManager
import com.example.breakingbad.view.viewmodel.CharactersListViewModel
import retrofit2.HttpException

class CharactersListFragment : Fragment() {

    private lateinit var root: View
    private lateinit var recyclerView: RecyclerView
    private lateinit var charactersListViewModel: CharactersListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        charactersListViewModel = ViewModelProvider(this).get(CharactersListViewModel::class.java)

        root = inflater.inflate(R.layout.fragment_characters_list, container, false)

        recyclerView = root.findViewById(R.id.recycler_view)

        setRecyclerViewAdapter()

        val searchView = root.findViewById<SearchView>(R.id.search_view)
        searchView.setOnClickListener {
            searchView.isIconified = false
        }
        searchView.queryHint = getString(R.string.search_for_a_character)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                (recyclerView.adapter as CharactersListRecyclerViewAdapter).filterByName(true)
                (recyclerView.adapter as CharactersListRecyclerViewAdapter).filter.filter(p0)
                return false
            }
        })

        val toast: Toast = Toast.makeText(context, "", Toast.LENGTH_SHORT)

        charactersListViewModel.charactersListResult.observe(requireActivity(), Observer {
            val charactersResult = it ?: return@Observer
            charactersResult.charactersResponse?.run {
                (recyclerView.adapter as CharactersListRecyclerViewAdapter).initList(this)
                recyclerView.onListLoaded {
                    setLayoutProgressBarVisibility(false)
                }
            }

            charactersResult.charactersError?.run {
                if (this is HttpException) {
                    val errorMessage = this.response()?.errorBody()?.string() ?: ""
                    toast.setText(errorMessage)
                    toast.show()
                }
            }

            charactersResult.isLoading?.run {
                setLayoutProgressBarVisibility(this)
            }

        })

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        charactersListViewModel.charactersRequest(context?.assets?.open("success_response.json"))
    }

    private fun setRecyclerViewAdapter() {
        //Setup layout manager
        val layoutManager = PreCachingLayoutManager(
            requireContext()
        )
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.setExtraLayoutSpace(PreCachingLayoutManager.DEFAULT_EXTRA_LAYOUT_SPACE)
        recyclerView.layoutManager = layoutManager

        recyclerView.adapter = CharactersListRecyclerViewAdapter(
            onItemClickListener
        )
    }

    fun filterBySeason(seasonNumber: Int) {
        (recyclerView.adapter as CharactersListRecyclerViewAdapter).filterByName(false)
        (recyclerView.adapter as CharactersListRecyclerViewAdapter).filter.filter(seasonNumber.toString())
    }

    private fun setLayoutProgressBarVisibility(isVisible: Boolean) {
        root.findViewById<ConstraintLayout>(R.id.layout_progress_bar).visibility =
            if (isVisible) View.VISIBLE else View.GONE
    }

    private var onItemClickListener: ((Character?) -> Unit) = {
        root.findViewById<SearchView>(R.id.search_view).onActionViewCollapsed()
        val bundle = bundleOf(Pair(resources.getString(R.string.character), it))
        findNavController().navigate(
            R.id.action_navigation_characters_list_to_navigation_character_details, bundle
        )
    }

    private fun RecyclerView.onListLoaded(action: () -> Unit) {
        val globalLayoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                action()
                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        }
        viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    }
}

