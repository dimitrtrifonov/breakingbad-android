package com.example.breakingbad.network

import com.example.breakingbad.model.Character
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {
    @GET("/api/characters")
    fun characters(): Observable<List<Character>>
}