package com.example.breakingbad.network

import com.example.breakingbad.model.Character
import com.google.gson.Gson
import io.reactivex.Single
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class ApiServiceMock {
    companion object {
        fun characters(inputStream: InputStream? = null): Single<List<Character>> {
            return Single.fromCallable {
                val buf = StringBuilder()
                inputStream?.let {
                    val bufferedReader = BufferedReader(InputStreamReader(inputStream))

                    var str: String? = bufferedReader.readLine()
                    while (str != null) {
                        buf.append(str)
                        str = bufferedReader.readLine()
                    }
                    inputStream.close()
                    bufferedReader.close()
                    Gson().fromJson(buf.toString(), Array<Character>::class.java)
                        .toList() as ArrayList<Character>
                }
            }
        }
    }
}