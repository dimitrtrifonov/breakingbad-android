package com.example.breakingbad

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.breakingbad.model.Character
import com.example.breakingbad.network.ApiService
import com.example.breakingbad.view.viewmodel.CharactersListViewModel
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CharactersListViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var charactersListViewModel: CharactersListViewModel
    private lateinit var charactersListViewModelWithApiService: CharactersListViewModel

    @Mock
    lateinit var apiService: ApiService

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        charactersListViewModel =
            CharactersListViewModel()
        charactersListViewModelWithApiService =
            CharactersListViewModel(
                apiService
            )
        val networkComponent =
            DaggerTestNetworkComponent.builder().build()
        networkComponent.inject(this)
    }

    @Test
    fun testCharactersListViewModelWithMockedJSON() {
        // Mock response with success
        Mockito.`when`(apiService.characters()).thenReturn(
            Observable.fromCallable {
                ApiUtils.getUrl<List<Character>>("success_response.json")
            }
        )
        charactersListViewModelWithApiService.charactersRequest()

        // Assertions
        assert(charactersListViewModelWithApiService.charactersListResult.value?.charactersResponse?.size==2)
    }

}