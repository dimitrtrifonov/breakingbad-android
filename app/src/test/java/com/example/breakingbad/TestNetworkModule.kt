package com.example.breakingbad

import com.example.breakingbad.BuildConfig.BASE_URL
import com.example.breakingbad.model.Character
import com.example.breakingbad.network.ApiService
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused", "UNUSED_PARAMETER")
class TestNetworkModule {
    @Provides
    @Singleton
    internal fun providePostApi(retrofit: Retrofit): ApiService {
        return object : ApiService {
            override fun characters(): Observable<List<Character>> {
                return Observable.fromCallable { ApiUtils.getUrl<List<Character>>("success_response.json") }
            }
        }
    }

    @Provides
    @Singleton
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL).build()
    }
}