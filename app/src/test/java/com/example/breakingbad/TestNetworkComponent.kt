package com.example.breakingbad

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestNetworkModule::class])
interface TestNetworkComponent {
    fun inject(charactersListViewModelTest: CharactersListViewModelTest)
}
